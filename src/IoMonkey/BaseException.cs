namespace IoMonkey 
{
    public class BaseException : System.Exception 
    {
        protected BaseException()
        {}

        protected BaseException(string message) 
            : base(message)
        {

        }
    }

}
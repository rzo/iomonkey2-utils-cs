namespace IoMonkey
{
    public class ExcessiveArraySizeException : BaseException
    {
        public int ItemCountLimit
        { get; private set; }
        public int ActualItemCount
        { get; private set; }

        public ExcessiveArraySizeException(int limit, int actualCount) 
            : base(string.Format("Array size ({0}) exceeds allowed limit ({1})", actualCount, limit))
        {
                ItemCountLimit = limit;
                ActualItemCount = actualCount;
        }
    }
}


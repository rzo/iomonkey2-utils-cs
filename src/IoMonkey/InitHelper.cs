﻿using System;
using System.Reflection;

namespace IoMonkey
{
    /// <summary>
    /// Initialization helper utility class.
    /// </summary>
    public static class InitHelper
    {
        /// <summary>
        /// Registers all IomReaderUtils Readers found in assembly.
        /// </summary>
        /// <param name="assembly">Assembly to be registered</param>
        public static void RegisterAssemblyReaders(Assembly assembly)
        {
            Type[] assemblyTypes = assembly.GetTypes();
            foreach (Type t in assemblyTypes)
            {
                if (t.Name == "IomReaderUtils")
                {
                    MethodInfo rr = t.GetMethod("RegisterReaders");
                    if (rr != null)
                        rr.Invoke(null, null);
                }
            }
        }

    }
}
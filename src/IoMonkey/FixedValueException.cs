namespace IoMonkey
{
    public class FixedValueException : BaseException
    {
        private object _expectedValue;
        private object _gotValue;
        
        private System.Type _ioClass;
        
        public object ExceptedValue
        { get { return _expectedValue; } }
        
        public object GotValue
        { get { return _gotValue; } }
        
        public System.Type IoClass
        { get { return _ioClass; } }

        private static string FormatMessage(object expectedValue, object gotValue, System.Type ioClass = null)
        {
            string ret = string.Format("Expected value '{0}' but got '{1}'", expectedValue, gotValue);
            
            if (ioClass != null)
            {
                ret += string.Format(" while loading {0}", ioClass.FullName);
            }
            
            return ret;
        }

        public FixedValueException()
        {}

        public FixedValueException(object expectedValue, object gotValue, System.Type ioClass = null)
            : base(FormatMessage(expectedValue, gotValue, ioClass))
        {
            _expectedValue = expectedValue;
            _gotValue = gotValue;
            _ioClass = ioClass;
        }
    }
}


using System.IO;

namespace IoMonkey
{
    public interface ILoadSave
    {
        void Load(BinaryReader r);
        void Save(BinaryWriter w);
    }
}

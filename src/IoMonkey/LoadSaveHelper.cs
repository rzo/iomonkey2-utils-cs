using System.Collections.Generic;
using System.IO;

namespace IoMonkey
{
    public class LoadSaveHelper
    {
        public static int ComputeLeb128ByteSize(int num) 
        {
            int ret = 1;
            while ((num >>= 7) > 0) ret++;
            return ret;
        }

        public static int ComputeStringStorage(string s) 
        {
            int n = System.Text.Encoding.UTF8.GetByteCount(s);
            return n + ComputeLeb128ByteSize(n);
        }

        public static void WriteNullIndices<T>(BinaryWriter writer, T[] vals)
        {
            short nullsCount = 0;
            for (int i = 0; i < vals.Length; i++) if (vals[i] == null) nullsCount++;
            writer.Write(nullsCount);
            for (int i = 0; i < vals.Length; i++) if (vals[i] == null) writer.Write(i);
        }

        public static void ReadNullIndices(BinaryReader reader, IList<int> outIndices)
        {
            short nullsCount = reader.ReadInt16();
            outIndices.Clear();
            while (nullsCount-- > 0) outIndices.Add(reader.ReadInt32());
        }

    }
}
